/**
 * @jsx React.h
 */
import React from 'preact'
import { Header } from '../ui'

const Loading = () => (
  <div id="page" className="mdl-layout mdl-js-layout mdl-layout--fixed-header">
    <Header />
    <main className="mdl-layout__content">
      <section id="loading">
        <center style="margin:50px auto;"><div className="mdl-spinner mdl-js-spinner is-active"></div></center>
      </section>
    </main>
  </div>
)

export default Loading
