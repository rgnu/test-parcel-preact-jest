/**
 * @jsx React.h
 */
import React from 'preact'
import { Header, Reloj, Input, Button } from '../ui'

export default class Home extends React.Component {
  constructor (props) {
    super(props)

    this.props.title = this.props.title || 'Home'

    this.state = {
      name: undefined,
      number: undefined
    }
  }

  onChangeName (name) {
    this.setState({ name })
  }

  onChangeNumber (number) {
    this.setState({ number })
  }

  render () {
    const { title, onSubmit } = this.props

    return (
      <div id="page" className="mdl-layout mdl-js-layout mdl-layout--fixed-header">
        <Header />
        <main className="mdl-layout__content">

          <section id="home">
            <form action="#" onSubmit={() => onSubmit(this.state)}>
              <ul className="mdl-list">
                <li className="mdl-list__item">
                  <h2>{title}</h2>
                </li>
                <li className="mdl-list__item">
                  <Reloj />
                </li>
                <li className="mdl-list__item">
                  <Input onChange={this.onChangeName.bind(this)} label="Name" pattern="[a-zA-Z ]+" error="Invalid Name" />
                </li>
                <li className="mdl-list__item">
                  <Input onChange={this.onChangeNumber.bind(this)} label="Number" pattern="[0-9]+" error="Number accept only numbers" />
                </li>
                <li className="mdl-list__item">
                  <Button text="Presioname" isEnabled={true}/>
                </li>
              </ul>
            </form>
          </section>
        </main>
      </div>
    )
  }
}

Home.propTypes = {
  title: String,
  onSubmit: Function
}
