/**
 * @jsx React.h
 */
import React from 'preact'
import { Header } from '../ui'

const NotFound = () => (
  <div id="page" className="mdl-layout mdl-js-layout mdl-layout--fixed-header">
    <Header />
    <main className="mdl-layout__content">
      <section id="not-found">
        <center><h3>Page Not Found</h3></center>
      </section>
    </main>
  </div>
)

export default NotFound
