/**
 * @jsx React.h
 */
import React from 'preact'
import { Header, Input, Button } from '../ui'

export default class Registro extends React.Component {
  constructor (props) {
    super(props)

    this.props.title = this.props.title || 'Registro'

    this.state = {
      name: undefined,
      phone: undefined,
      email: undefined
    }
  }

  onChangeName (name) {
    this.setState({ name })
  }

  onChangePhone (phone) {
    this.setState({ phone })
  }

  onChangeEmail (email) {
    this.setState({ email })
  }

  isValid () {
    return this.state.name && this.state.phone && this.state.email
  }

  render () {
    const { title, onSubmit } = this.props
    const isEnabled = this.isValid()

    return (
      <div id="page" className="mdl-layout mdl-js-layout mdl-layout--fixed-header">
        <Header />
        <main className="mdl-layout__content">
          <section id="registro">
            <form onSubmit={() => onSubmit(this.state)}>
              <ul className="mdl-list">
                <li className="mdl-list__item">
                  <h3>{title}</h3>
                </li>
                <li className="mdl-list__item">
                  <Input onChange={this.onChangeName.bind(this)} label="Nombre y Apellido" pattern="[a-zA-Z ]+" error="El nombre o apellido inválido" />
                </li>
                <li className="mdl-list__item">
                  <Input onChange={this.onChangePhone.bind(this)} label="Número de Celular" pattern="[0-9\+\- ]+" error="Número de teléfono inválido" />
                </li>
                <li className="mdl-list__item">
                  <Input onChange={this.onChangeEmail.bind(this)} label="Email" pattern="[0-9a-zA-Z_\.\-\+@]+" error="Email inválido" />
                </li>
                <li className="mdl-list__item">
                  <Button text="Registrarse" isEnabled={isEnabled} />
                </li>
              </ul>
            </form>
          </section>
        </main>
      </div>
    )
  }
}

Registro.propTypes = {
  title: String,
  onSubmit: Function
}
