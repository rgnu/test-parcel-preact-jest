export { default as Home } from './Home'
export { default as Registro } from './Registro'
export { default as NotFound } from './NotFound'
export { default as Loading } from './Loading'
