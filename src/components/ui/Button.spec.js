/**
 * @jsx React.h
 */
import React from 'preact'
import Button from './Button'
import { simulate } from '../../utils'

describe('Button', async () => {
  const text = 'Test Button'

  it('render', async () => {
    const div = document.createElement('div')
    const props = { text, onClick: jest.fn() }

    // eslint-disable-next-line react/no-deprecated
    React.render(<Button {...props} />, div)

    // We expect the task titled "Task 6 (pinned)" to be rendered first, not at the end
    const button = div.querySelector('button')

    await simulate(button, 'click')

    expect(button).not.toBe(null)
    expect(button.innerHTML).toBe(text)
    expect(props.onClick).toBeCalled()
  })
})
