/**
 * @jsx React.h
 */
import React from 'preact'

const Input = ({ onChange = () => {}, label = '', pattern = '.+', error }) => {
  return (
    <div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
      <input className="mdl-textfield__input" type="text" pattern={pattern} onChange={(e) => onChange(e.target.value)} />
      <label className="mdl-textfield__label">{label}</label>
      <span className="mdl-textfield__error">{error}</span>
    </div>
  )
}

Input.propTypes = {
  label: String,
  pattern: String,
  error: String,
  onChange: Function
}

export default Input
