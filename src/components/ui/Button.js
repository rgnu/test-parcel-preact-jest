/**
 * @jsx React.h
 */
import React from 'preact'

const Button = ({ onClick, text, isEnabled }) => (
  <button disabled={!isEnabled} className="mdl-button mdl-button--colored mdl-button--raised mdl-js-button mdl-js-ripple-effect" onClick={onClick}>
    {text}
  </button>
)

Button.propTypes = {
  onClick: Function,
  text: String,
  isEnabled: Boolean
}

export default Button
