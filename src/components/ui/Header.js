// @jsx React.h
import React from 'preact'
// eslint-disable-next-line
import HeaderStyle from './Header.css'

import { Link } from 'preact-router/match'

const Header = ({ title = 'Pignus' }) => (
  <header className="mdl-layout__header">
    <div className="mdl-layout-icon">
      <Link href="/">
        <div id="logo" className="avatar"/>
      </Link>
    </div>
    <div className="mdl-layout__header-row">
      <span className="mdl-layout__title">{title}</span>
      <div className="mdl-layout-spacer"></div>
    </div>
  </header>
)

Header.propTypes = {
  title: String
}

export default Header
