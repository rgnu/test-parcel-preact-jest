/**
 * @jsx React.h
 */
import React from 'preact'

const Reloj = () => {
  let time = new Date().toLocaleTimeString()

  return (
    <div>
      {time}
    </div>
  )
}

export default Reloj
