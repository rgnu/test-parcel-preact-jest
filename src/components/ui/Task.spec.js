/**
 * @jsx React.h
 */
import React from 'preact'
import Task from './Task'

describe('Task', async () => {
  const task = {
    id: '1',
    title: 'Test Task',
    state: 'TASK_INBOX',
    updatedAt: new Date(2018, 0, 1, 9, 0)
  }

  it('render', async () => {
    const div = document.createElement('div')
    const events = { onPinTask: jest.fn(), onArchiveTask: jest.fn() }

    // eslint-disable-next-line react/no-deprecated
    React.render(<Task task={task} {...events} />, div)

    // We expect the task titled "Task 6 (pinned)" to be rendered first, not at the end
    const lastTaskInput = div.querySelector('.list-item:nth-child(1) > .title > input')

    expect(lastTaskInput).not.toBe(null)
    expect(lastTaskInput.value).toBe(task.title)

    const archiveButton = div.querySelector('.list-item:nth-child(1) > .actions > a')

    expect(archiveButton).not.toBe(null)
  })
})
