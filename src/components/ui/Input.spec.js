/**
 * @jsx React.h
 */
import React from 'preact'
import Input from './Input'
import { simulate } from '../../utils'

describe('Input', async () => {
  it('render', async () => {
    const div = document.createElement('div')
    const inputValue = 'Test Value'
    const props = { label: 'Test Input', pattern: '[0-9]+', error: 'Input value invalid', onChange: jest.fn() }

    // eslint-disable-next-line react/no-deprecated
    React.render(<Input {...props} />, div)

    const label = div.querySelector('div > label')

    expect(label).not.toBe(null)
    expect(label.innerHTML).toBe(props.label)

    const error = div.querySelector('div > span.mdl-textfield__error')

    expect(error).not.toBe(null)
    expect(error.innerHTML).toBe(props.error)

    const input = div.querySelector('div > input')

    expect(input).not.toBe(null)

    input.value = inputValue

    await simulate(input, 'change')

    expect(props.onChange).toBeCalledWith(inputValue)
  })
})
