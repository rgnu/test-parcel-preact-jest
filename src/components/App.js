/** @jsx React.h */
import React from 'preact'
import { Router, route } from 'preact-router'
import { Home, Registro, NotFound, Loading } from './pages'
import { createHashHistory } from 'history'

export default class App extends React.Component {
  onRegistroSubmit (data) {
    console.log(data)
    setTimeout(() => {
      console.log('Goto Home')
      route('/')
    }, 1000)
    route('/loading')
  }

  render () {
    return (
      <Router history={createHashHistory()} >
        <Registro path="/registro" onSubmit={this.onRegistroSubmit} />
        <Home path="/" onSubmit={this.onRegistroSubmit} />
        <Loading path="/loading" />
        <NotFound default />
      </Router>
    )
  }
}
