export const simulate = (node, event) => {
  // pass a selector or a dom node
  if (typeof node === 'string') {
    node = document.querySelector(node)
  }

  // pass an event object or an event name
  event = event instanceof window.Event ? event : new window.Event(event)
  node.dispatchEvent(event)
  // return a promise that resolves on next tick so the invoking code see the update
  return new Promise(resolve => {
    process.nextTick(() => { resolve() })
  })
}
