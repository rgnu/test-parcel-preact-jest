/** @jsx h */
// eslint-disable-next-line no-unused-vars
import classes from '~/css/main.css'
// eslint-disable-next-line no-unused-vars
import materialJs from 'material-design-lite/dist/material'

import 'preact/devtools'

import { h, render } from 'preact'

import { App } from './components'

const root = document.body

render(<App />, root)
